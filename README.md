## RabbitMQ в Docker

###Работа с контейнером

Предвариатнльно нужно установить docker и docker-compose, клонировать репозиторий 

Работа с docker-compose производится из папки с фалом docker-compose.yaml (папка проекта)

Запуск контейнера:
```bash
docker-compose up -d --build
```

Остановка контейнера:
```bash
docker-compose down
```

После презапуска контейнера все данные будут потеряны

Порты: 15672, 5672

###Web интерфейс RabbitMQ

Web интерфейс RabbitMQ доступен по адресу http://{SERVER_IP}:15672/

Логин и пароль guest/guest